# OpenML dataset: data-v3.en-es-lit.clean.anno_without_14top.uniform.arff

https://www.openml.org/d/40742

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

DBpedia mappings EN-ES-literals. Dataset used to train a predictive model. Uses the annotations provided manually (240) - 14 (used to validate the model). That is, 226 annotations.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40742) of an [OpenML dataset](https://www.openml.org/d/40742). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40742/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40742/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40742/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

